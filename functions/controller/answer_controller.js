var admin = require("firebase-admin");

// var quizzModels = require("./../models/quizz.js");

module.exports = {
    initAnswer: async function (req, res) {
        const { userID, quizzID } = req.params;

        let queryQuizz = await admin.firestore().collection("Quizz").doc(quizzID).get();

        let dataToAdd = {}

        let queryExist = await admin.firestore().collection("AnswerUser").doc(userID).get();

        for (const answer of  queryQuizz.data().answer_list) {

            const answerID = answer.id;

            


            if (!queryExist.exists) {
                dataToAdd[answerID] = { "points": 0 };
            } else {
                if (queryAnswerExist[answerID] == undefined) {
                    dataToAdd[answerID] = { "points": 0 };
                }
            }
        }

        if (Object.keys(dataToAdd).length !== 0) {
            if (!queryExist.exists) {
                await admin.firestore().collection("AnswerUser").doc(userID).set(dataToAdd);
            } else {
                await admin.firestore().collection("AnswerUser").doc(userID).update(dataToAdd);
            }

        }

        res.send({ message: "success" });
    },

    points: async function (req, res) {
        const { userID, answerID } = req.params;
        const oppositeAnswerID = req.query.oppositeAnswerID;

        pointsToAdd = req.body.points

        pointsBefore = await (await admin.firestore().collection("AnswerUser").doc(userID).get()).data()[answerID].points;

        await admin.firestore().collection("AnswerUser").doc(userID).update({[answerID]: {points: pointsBefore + pointsToAdd}});

        if(oppositeAnswerID != undefined){
            let pointsToAddOpposite = 0;
            switch(pointsToAdd){
                case 1: pointsToAddOpposite = 4;
                break;
                case 2: pointsToAddOpposite = 3;
                break;
                case 3: pointsToAddOpposite = 2;
                break;
                case 4: pointsToAddOpposite = 1;
                break
            }

            pointsBeforeOpposite = await (await admin.firestore().collection("AnswerUser").doc(userID).get()).data()[oppositeAnswerID].points;

            await admin.firestore().collection("AnswerUser").doc(userID).update({[oppositeAnswerID]: {points: pointsBeforeOpposite + pointsToAddOpposite}});
        }

        
        res.send({message: "success"});
    },
};

