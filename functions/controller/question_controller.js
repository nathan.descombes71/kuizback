var admin = require("firebase-admin");

// var quizzModels = require("./../models/quizz.js");

module.exports = {
    initQuestion: async function (req, res) {
        const { userID, quizzID } = req.params;

        let queryQuizz = await admin.firestore().collection("Quizz").doc(quizzID).get();

        let dataToAdd = {}

        let queryExist = await admin.firestore().collection("QuestionUser").doc(userID).get();
        queryQuestionExist = (await admin.firestore().collection("QuestionUser").doc(userID).get()).data();

        for (const question of queryQuizz.data().question_list) {

            const questionID = question.id;

           

            if (!queryExist.exists) {
                dataToAdd[questionID] = { "isAnswered": false };
            } else {
                if (queryQuestionExist[questionID] == undefined) {
                    dataToAdd[questionID] = { "isAnswered": false };
                }
            }
        }




        console.log("HERE");
        console.log(Object.keys(dataToAdd));
        if (Object.keys(dataToAdd).length !== 0) {
            if (!queryExist.exists) {
                await admin.firestore().collection("QuestionUser").doc(userID).set(dataToAdd);
            } else {
                await admin.firestore().collection("QuestionUser").doc(userID).update(dataToAdd);
            }
        }


        res.send({ message: "success" });
    },
    questionAnswered: async function (req, res) {
        const { userID, questionID } = req.params;

        await admin.firestore().collection("QuestionUser").doc(userID).update({[questionID]: {isAnswered: true}});

        res.send({message: "success"});
    },
    addQuestion: async function (req, res) {
        const { quizzID } = req.params;

        await admin.firestore().collection("Quizz").doc(quizzID).update({question_list: req.body});

        res.send({ message: "Success" });
    },
};

