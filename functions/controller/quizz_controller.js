var admin = require("firebase-admin");

var quizzModels = require("./../models/quizz.js");

module.exports = {
    showQuizz: async function (req, res) {
        const { id } = req.params;
        const userId = req.query.userId;

        let dataToReturn = {};

        let query = await admin.firestore().collection("Quizz").doc(id).get();

        if (!query.exists) {
            res.status(404).send({ error: "quizz-not-found" });
        }

        dataToReturn = quizzModels.quizzToJsonWithAll(query);

        let questionList = dataToReturn.question_list;
        let answerList = dataToReturn.answer_list;

        let newQuestionList = await questionList.map(async function (v) {
            let elementID = v.id;
            let isAnswered = (await admin.firestore().collection("QuestionUser").doc(userId).get()).data()[elementID].isAnswered;
            return ({ ...v, isAnswered: isAnswered })
        })

        let newAnswerList = await answerList.map(async function (v) {
            let elementID = v.id;
            let points = (await admin.firestore().collection("AnswerUser").doc(userId).get()).data()[elementID].points;
            return ({ ...v, points: points })
        })

        dataToReturn.question_list = await Promise.all(newQuestionList).then((values) => { return values });

        dataToReturn.answer_list = await Promise.all(newAnswerList).then((values) => { return values });

        res.send(dataToReturn);
    },
    allQuizz: async function (req, res) {

        let dataToReturn = [];

        let query = await admin.firestore().collection("Quizz").get();

        query.forEach(function (doc) {
            dataToReturn.push(quizzModels.quizzToJsonWithoutQuery(doc));
        });

        res.send(dataToReturn);
    },
};

