const functions = require("firebase-functions");
const express = require('express');
var admin = require("firebase-admin");
const cors = require('cors');

const app = express();
var quizzController = require("./controller/quizz_controller.js");
var questionController = require("./controller/question_controller.js");
var answerController = require("./controller/answer_controller.js");

admin.initializeApp();

app.get('/healthcheck', (req, res) => {
    res.status(200).send("OK !");
});

app.get('/quizz/:id', (req, res) => {
    quizzController.showQuizz(req, res);
});

app.get("/quizz", (req, res) => {
    quizzController.allQuizz(req, res);
});

app.post("/question/:userID/:quizzID", (req, res) => {
    questionController.initQuestion(req, res);
});

app.put("/quizz/:quizzID", (req, res) => {
    questionController.addQuestion(req, res);
});

app.post("/answer/:userID/:quizzID", (req, res) => {
    answerController.initAnswer(req, res);
});

app.post("/points/:userID/:answerID", (req, res) => {
    answerController.points(req, res);
});

app.post("/answerQuestion/:userID/:questionID", (req, res) => {
    questionController.questionAnswered(req, res);
});


exports.v1 = functions.https.onRequest(app);