var admin = require("firebase-admin");


var answerModel = require("./answer.js");

module.exports = {
    quizzToJsonWithoutQuery: function (query) {
        return {
            "id": query.id ?? "",
            "label": query.data().label ?? "",
            "path_photo": query.data().path_photo ?? "",
            // "answer_list": query.data().answer_list ?? [],
            // .forEach(element => {
            //     let listToReturn = [];

            //     listToReturn.push(answerModel.answerToJson(element))

            //     return listToReturn;
            // }) ?? [],
        }
    },
    quizzToJsonWithAll: function (query) {
        return {
            "id": query.id ?? "",
            "label": query.data().label ?? "",
            "path_photo": query.data().path_photo ?? "",
            "answer_list": query.data().answer_list ?? [],
            "question_list": query.data().question_list ?? [], 
            "subtitle": query.data().subtitle ?? "",
            // .forEach(element => {
            //     let listToReturn = [];

            //     listToReturn.push(answerModel.answerToJson(element))

            //     return listToReturn;
            // }) ?? [],
        }
    },
};
