var admin = require("firebase-admin");


module.exports = {
    answerToJson: function (query) {
        return {
            "id": query.id ?? "",
            "label": query.data().label_answer ?? "",
            "path_photo" : query.data().path_photo ?? ""
        }
    },
};
